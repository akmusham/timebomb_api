var Datastore = require('nedb')
const path = require('path')
const os = require('os')
// const sqlite3 = require('sqlite3').verbose();
const express = require('express')
const eventEmitter = require('events')
const bodyParser = require('body-parser')
const cors = require('cors')
const routes = require('./Routes')
const fs = require('fs')
const store = require('./store.json')
const app = express()
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
app.use(cors())



global.users = new Datastore({filename: 'users',autoload: true})
global.authUsers = new Datastore({filename: 'authUsers',autoload: true})
global.vistors = new Datastore({filename: 'vistors',autoload: true})

// global.userTimeBomb = new Datastore()

app.use((req, res, next) => {
  res.set({
    "X-Frame-Options": "sameorigin",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    "Cache-Control": "max-age=10,max-stale=10,min-fresh=10,no-cache,no-store,no-transform,only-if-cached ",
    "strict-transport-security": "max-age=31536000,includeSubDomains,preload",
    "X-XSS-Protection": "1; mode=block",
    "x-content-type-options": "nosniff"
  })
  next()
})


// app.use(bodyParser.json({ type: 'application/*+json' }))

app.use('/timebomb/api/v1', routes)

// app.post('/yolo',function (req,res) {
//   console.log('resss',req.body);
//
// })

httpServer = require('http').createServer(app)

httpServer.timeout = 0;

httpServer.listen(process.env.timeBomb_app_api_port || 8977, async() => {
  console.log(`Server listening on port ${process.env.timeBomb_app_api_port || 8977}`)
  // let db = new sqlite3.Database(path.resolve(__dirname, './timebomb.sql'), (err) => {
  // if (err) {
  //   return console.error(err.message);
  // }
  // console.log('Connected to the in-memory SQlite database.');
  // });
  //
  // global.db = db
  // let users = await users.find({})
}).on('error', function(err) {
  console.log(err)
})


module.exports = httpServer
