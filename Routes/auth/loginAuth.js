module.exports = async (req, res) => {
  try {
    console.log('req-=-=-=-=-=-=',req.body);
    let {email,pass} = req.body
    global.users.findOne({ email: email,pass:pass }, function (err, doc) {
      console.log('doc',doc);
        if (doc !== null) {
          res.status(200).json({
            'status': true,
            'response': 'Authenticated',
            'data': doc
          })
        }else {
          res.status(401).json({
            'status': false,
            'response': 'Invalid user!'
          })
        }
      });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      'response': err
    })
  }
}
