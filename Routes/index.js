const express = require('express')
const router = express.Router()

router.post('/addTs',require('./users/addTs'))
router.get('/users',require('./users'))
router.get('/usersList',require('./users/users'))
router.post('/users',require('./users/add'))
router.post('/auth',require('./auth'))
router.post('/authLogin',require('./auth/loginAuth'))
router.post('/UpdateVisitor',require('./users/updateVisitor'))


module.exports = router
